﻿#include <iostream>
#include <string>
#include <algorithm>
#include <cstdlib>
class NameScore
{
private:
    std::string Name;
    int Score;
public:
    friend bool comp(const NameScore&, const NameScore&);
    void SetName(std::string NewName)
    {
        Name = NewName;
    }
    void SetScore(int NewScore)
    {
        Score = NewScore;
    }
    std::string GetName()
    {
        return Name;
    }
    int GetScore()
    {
        return Score;
    }
};
bool comp (const NameScore& a, const NameScore& b)
{
    return a.Score > b.Score;
}

int main()
{
    int KolvoNames;
    std::cout << "Kolichestvo Names: " << "\n";
    std::cin >> KolvoNames;

    NameScore* NamesScores = new NameScore[KolvoNames];

    for (int i = 0; i < KolvoNames; i++)
    {
        std::string Name;
        int Score;
        std::cout << "Vvedite Name: ";
        std::cin >> Name;
        NamesScores [i].SetName(Name);
        std::cout << "Vvedite Score: ";
        std::cin >> Score;
        NamesScores[i].SetScore(Score);
    }
    std::sort(NamesScores, NamesScores + KolvoNames, comp);
    for (size_t i = 0; i < KolvoNames; i++)
    {
        std::cout << NamesScores[i].GetName() << " " << NamesScores[i].GetScore() << '\n';
    }
    delete[] NamesScores;
}